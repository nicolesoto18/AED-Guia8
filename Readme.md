				    
                                                        	   Métodos de ordenamiento
							        ---------------------------
+ Empezando
    El programa consiste en la comparación del tiempo de procesamiento de 7 métodos de ordenamiento, al ejecutar debe ingresar 2 parámetros el uno corresponde al tamaño del arreglo que quiere ordenar y el segundo es para decidir si ver o no el contenido del arreglo antes de ser ordenado, los números del arreglo se ingresaran de forma aleatoria. Si ingreso s en el segundo parámetro la sálida sera el arreglo desordenado junto con la tabla de comparación de tiempos, en cambio si ingreso n solo vera la tabla de los tiempos de ordenamiento.

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro1 parámetro2
	
	parámetro1 --> Debe ser un número
	parámetro2 --> Debe ser s o n (s mostrara el arreglo desordenado y el tiempo de ordenamiento, en cambio n solo el tiempo)

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.
    graphviz: Paquete de visualización.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.

