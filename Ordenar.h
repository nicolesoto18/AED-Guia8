#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Ordenar{

    private:

    public:
        Ordenar();
        void burbujaMayor(int* arreglo, int tamano);
        void burbujaMenor(int* arreglo, int tamano);
        void imprimir(int* arreglo, int tamano);
        void insercion(int* arreglo, int tamano);
        void seleccion(int* arreglo, int tamano);
        void insercionBinaria(int* arreglo, int tamano);
        void shell(int* arreglo, int tamano);
        double quicksort(int* arreglo, int tamano);
        void reduceQuicksort(int* arreglo, int inicio, int fin, int &pos);

};
#endif
    
