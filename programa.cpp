#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "Ordenar.h"// Definición de la clase

using namespace std;

/*void mostrar_ordenamiento(Ordenar ordenar, int* arreglo, int tamano){ // Revisar contenido ordenado
    cout << "mayor" << endl;
    ordenar.burbujaMayor(arreglo, tamano);

    cout << "menor" << endl;
    ordenar.burbujaMenor(arreglo, tamano);

    cout << "inserción" << endl;
    ordenar.insercion(arreglo, tamano);
    
    cout << "selección" << endl;
    ordenar.seleccion(arreglo, tamano);
    
    cout << "binario" << endl;
    ordenar.insercionBinaria(arreglo, tamano);
    
    cout << "shell" << endl;
    ordenar.shell(arreglo, tamano);

    cout << "quicksort" << endl;
    ordenar.quicksort(arreglo, tamano);
}*/  


void mostrar_arreglo(int* arreglo, int tamano){
    cout << "   El contenido del arreglo desordenado es:" << endl;

    for(int i = 0; i < tamano; i++){
        cout << "   " << arreglo[i] << "  -";
    }
    cout << '\n' << endl;
}

// Mostrara el tiempo que tarda cada método   
void mostrar_tiempo(int *aux1, int *aux2, int *aux3, int *aux4, int *aux5, int *aux6, int *aux7, int tamano, Ordenar ordenar){
    double tiempoQuicksort;
        
    cout << "   ------------------------------------------" << endl;
    cout << "   Método              | Tiempo" << endl;
    cout << "   ------------------------------------------" << endl;
    ordenar.burbujaMenor(aux1, tamano);
    ordenar.burbujaMayor(aux2, tamano);
    ordenar.insercion(aux3, tamano);
    ordenar.seleccion(aux4, tamano);
    ordenar.insercionBinaria(aux5, tamano);
    ordenar.shell(aux6, tamano);

    tiempoQuicksort = ordenar.quicksort(aux7, tamano);
    cout << "   Quicksort           | " << tiempoQuicksort << " milisegundos\n" << endl; 
}


int validarTamano(int tamano, char *argv[]){
    int num;
    Ordenar ordenar = Ordenar();     
    string ver =  argv[2];
    
    if(tamano > 0 && tamano <= 1000000){ // El número es correcto
        int aux[tamano];
        int arrMenor[tamano];
        int arrMayor[tamano];
        int arrInsercion[tamano];
        int arrSeleccion[tamano];
        int arrBinaria[tamano];
        int arrShell[tamano];
        int arrQuicksort[tamano];

        for(int i = 0; i < tamano; i++){
            aux[i] = (rand() % tamano) + 1; // Rellena el arreglo con números aleatorios
        }
       
         
        for(int j = 0; j < tamano; j++){ // Se crean arreglos iguales al original
            num = aux[j];
            arrMenor[j] = num;
            arrMayor[j] = num;
            arrInsercion[j] = num;
            arrSeleccion[j] = num;
            arrBinaria[j] = num;
            arrShell[tamano] = num;
            arrQuicksort[j] = num;
        }

        if(ver == "s"){ // validar parámetro dos
            mostrar_arreglo(aux, tamano); // Se muestra el arreglo original
            mostrar_tiempo(arrMenor, arrMayor, arrInsercion, arrSeleccion, arrBinaria, arrShell, arrQuicksort, tamano, ordenar);
        }

        else if(ver == "n"){
            mostrar_tiempo(arrMenor, arrMayor, arrInsercion, arrSeleccion, arrBinaria, arrShell, arrQuicksort, tamano, ordenar);
        }

        else{
            cout << "FATAL: Debe ingresar s o n en el segundo parámetro para ver o no el contenido del arreglo desordenado" << endl;
            cout << "s --> Muestra el arreglo desordenado y el tiempo de ordenamiento" << endl;
            cout << "n --> Solo muestra el tiempo de ordenamiento" << endl;
            return -1;
        }
    }
    
    else{ 
        cout << "FATAL: Número inválido, abra el programa de nuevo" << endl;
        cout << "Recuerdo que ingresar un número distinto" << endl;
        return -1;
    }
}

int validarParametros(int argc, char *argv[]){
    if(argc == 3){
        int tamano;

        try{ // Validar el ingreso de números
            tamano = stoi(argv[1]); // Cambia el caracter recibido a un número
            validarTamano(tamano, argv);

        }

        catch(const std::invalid_argument& e){
            cout << "Fatal: Como primer parámetro debe ingresar un número" << endl;
            return -1;
        }
    }

    else{
        cout << "FATAL: No ingreso la cantidad suficiente de parámetros\nReinicie el programa" << endl;
        return -1;
    }
}

int main(int argc, char *argv[]){
    int iniciar;
    
    cout << "\n\tMétodos de ordenamiento" << endl;
    cout << "     -------------------------------\n" << endl;
    iniciar = validarParametros(argc, argv); // validación de la entrada de parámetros

    return 0;
}
