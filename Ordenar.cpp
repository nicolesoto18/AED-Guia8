#include <iostream>
#include "Ordenar.h"

Ordenar::Ordenar(){
}

void Ordenar::imprimir(int* arreglo, int tamano){ // Imprime el arreglo ordenado
	for (int i=0; i < tamano; i++){
		cout << arreglo[i] << " - " << endl;		
	}
}


void Ordenar::burbujaMenor(int* arreglo, int tamano){
	int aux;
    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();

	for (int i = 0; i < tamano; i++){
		for (int j = 0; j < tamano-1; j++){
			if (arreglo[j] > arreglo[j+1]){ // Busca los mayores
				aux = arreglo[j]; 
				arreglo[j] = arreglo[j+1]; // Mueve los números a la derecha
				arreglo[j+1] = aux;
			}
		}	
	}

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000; // Calcula el tiempo que tarda
    cout << "   Burbuja menor       | " << tiempo << " milisegundos" << endl; 
}


void Ordenar::burbujaMayor(int* arreglo, int tamano){
	int aux;
    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();
	
	for (int i = 0; i < tamano; i++){
		for (int j = 0; j < tamano-1; j++){
			if (arreglo[j] < arreglo[j+1]){ // Busca los números menores 
				aux = arreglo[j]; 
				arreglo[j] = arreglo[j+1]; // Mueve los menores a la derecha
				arreglo[j+1] = aux;
			}
		}	
	}

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000; //Calcula el tiempo que tarda
    cout << "   Burbuja mayor       | " << tiempo << " milisegundos" << endl; 
}

void Ordenar::insercion(int* arreglo, int tamano){
    int aux;
	int i;
    int j;

    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();

	for (i = 0; i < tamano; i++){
		aux = arreglo[i];
		j = i - 1;

		while ((arreglo[j] > aux) && (j >= 0) ){ // Se matiene mientras los números mayores enten a la izquierda
			arreglo[j+1] = arreglo[j]; // Mueve los números mayores a la derecha
			j--;
			arreglo[j+1] = aux;
		}
	}

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    cout << "   Inserción           | " << tiempo << " milisegundos" << endl; 
}


void Ordenar::seleccion(int* arreglo, int tamano){
	int aux;
    int menor;
    int k;
    
    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();

    for (int i = 0; i < tamano ; i++) {
        menor = arreglo[i];
        k = i;
    
        for (int j = i + 1; j < tamano; j++) { // Parte una posición más a la derecha
            if (arreglo[j] < menor){ //Busca el número más pequeño
                menor = arreglo[j];
                k = j;
		   }
       }

       arreglo[k] = arreglo[i]; // Mueve el número a la siquiente posición (dejando los menores a la izquierda)
       arreglo[i] = menor;
	}

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    cout << "   Selección           | " << tiempo << " milisegundos" << endl; 
}


void Ordenar::insercionBinaria(int* arreglo, int tamano){
    int aux;
    int izq;
    int der;
    int mitad;
    int j;

    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();

    for(int i = 1; i < tamano; i++){
        aux = arreglo[i];
        izq = 1;
        der = i - 1;

        while(izq <= der){
            mitad = (izq + der)/2; // Busca la posición central 
            if(aux <= arreglo[mitad]){
                der = mitad - 1; 
            }

            else{
                izq = mitad + 1;
            }
        }
        
        j = i - 1;
        while(j >= izq){
            arreglo[j+1] = arreglo[j]; // Se mueven a la derecha para insertar el número a ordenar
            j = j - 1;
        }

        arreglo[izq] = aux;
    }

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    cout << "   Inserción Binaria   | " << tiempo << " milisegundos" << endl; 
}

            
void Ordenar::shell(int* arreglo, int tamano){
	bool bandera;
	int entero;
	int aux;
	int i;

    double tiempo = 0;
	clock_t comienzo;
    comienzo = clock();

	entero = tamano + 1;

	while (entero > 1){
		entero = entero/2;
		bandera = true;

		while (bandera == true){
			bandera = false;
			i = 0;

			while ((i + entero) < tamano){
				if (arreglo[i] > arreglo[i + entero]){
					aux = arreglo[i];
					arreglo[i] = arreglo[(i + entero)];
					arreglo[(i + entero)] = aux;
					bandera = true;
				}

                i = i + 1;
            }
        }
	}

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    cout << "   Shell               | " << tiempo << " milisegundos" << endl; 
}


void Ordenar::reduceQuicksort(int* arreglo, int inicio, int fin, int &pos){
    int aux;
    int izq;;
    int der;
    bool bandera;
    
    izq = inicio;
    der = fin;
    bandera = true;
    pos = inicio;

    while(bandera == true){
        while(arreglo[pos] <= arreglo[der] && pos != der){
            der--;
        }

        if(pos == der){
            bandera = false;
        }

        else{
            aux = arreglo[pos];
            arreglo[pos] = arreglo[der];
            arreglo[der] = aux;
            pos = der;

            while(arreglo[pos] >= arreglo[izq] && pos != izq){
                izq++;
                aux = arreglo[pos];
                arreglo[pos] = arreglo[izq];
                arreglo[izq] = aux;
                pos = izq;
            }
        }
    }
}


double Ordenar::quicksort(int* arreglo, int tamano){
    int pilaMenor[tamano];
    int pilaMayor[tamano];
    int inicio;
    int fin;
    int pos;
    int tope = 0;

    double tiempo;
    clock_t comienzo;
	comienzo = clock();

    pilaMenor[tope] = 0;
    pilaMayor[tope] = tamano-1;
    
    while(tope >= 0 ){
        inicio = pilaMenor[tope];
        fin = pilaMayor[tope];
        tope--;

        reduceQuicksort(arreglo, inicio, fin, pos);

        if(inicio < (pos-1)){
            tope++;
            pilaMenor[tope] = inicio;
            pilaMayor[tope] = pos-1;
        }

        if(fin > (pos+1)){
            tope++;
            pilaMenor[tope] = pos+1;
            pilaMayor[tope] = fin;
        }
    }

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    return tiempo;
}
